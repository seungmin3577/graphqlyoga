import {getUsers, getUserById, createUser, deleteUser} from '../DataBase/db';

const resolvers = {
    Query:{
        users: () => getUsers(),
        user:(_, {id}) => getUserById(id),
    },
    Mutation:{
        createUser: (_, {id, email, password, name, type}) => createUser(id, email, password, name, type),
        deleteUser: (_, {id}) => deleteUser(id),
    }
};

export default resolvers;